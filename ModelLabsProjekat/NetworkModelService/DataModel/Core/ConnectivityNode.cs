﻿using FTN.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FTN.Services.NetworkModelService.DataModel.Core
{
    public class ConnectivityNode : IdentifiedObject
    {
        private long connectivityNodeContainer = 0;
        private long topologicalNode = 0;
        private List<long> terminals = new List<long>();

        public ConnectivityNode(long globalId) : base(globalId)
        {
        }

        public long ConnectivityNodeContainer
        {
            get { return connectivityNodeContainer; }
            set { connectivityNodeContainer = value; }
        }

        public long TopologicalNode
        {
            get { return topologicalNode; }
            set { topologicalNode = value; }
        }

        public List<long> Terminals
        {
            get { return terminals; }
            set { terminals = value; }
        }

        public override bool Equals(object obj)
        {
            if (base.Equals(obj))
            {
                ConnectivityNode x = (ConnectivityNode)obj;
                return (x.connectivityNodeContainer == this.connectivityNodeContainer && x.topologicalNode == this.topologicalNode && CompareHelper.CompareLists(x.terminals, this.terminals));
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        #region IAccess implementation

        public override bool HasProperty(ModelCode property)
        {
            switch (property)
            {
                case ModelCode.CONNNODE_CONTAINER:
                case ModelCode.CONNNODE_TERMINALS:
                case ModelCode.CONNNODE_TOPOLONODE:
                    return true;

                default:
                    return base.HasProperty(property);
            }
        }

        public override void GetProperty(Property property)
        {
            switch (property.Id)
            {
                case ModelCode.CONNNODE_CONTAINER:
                    property.SetValue(connectivityNodeContainer);
                    break;
                case ModelCode.CONNNODE_TERMINALS:
                    property.SetValue(terminals);
                    break;
                case ModelCode.CONNNODE_TOPOLONODE:
                    property.SetValue(topologicalNode);
                    break;
                default:
                    base.GetProperty(property);
                    break;
            }
        }


        public override void SetProperty(Property property)
        {
            switch (property.Id)
            {
                case ModelCode.CONNNODE_CONTAINER:
                    connectivityNodeContainer = property.AsReference();
                    break;

                case ModelCode.CONNNODE_TOPOLONODE:
                    topologicalNode = property.AsReference();
                    break;

                default:
                    base.SetProperty(property);
                    break;
            }
        }


        #endregion IAccess implementation

        #region IReference implementation

        public override bool IsReferenced
        {
            get
            {
                return terminals.Count > 0 || base.IsReferenced;
            }
        }

        public override void GetReferences(Dictionary<ModelCode, List<long>> references, TypeOfReference refType)
        {
            if (terminals != null && terminals.Count > 0 && (refType == TypeOfReference.Target || refType == TypeOfReference.Both))
            {
                references[ModelCode.CONNNODE_TERMINALS] = terminals.GetRange(0, terminals.Count);
            }

            if (connectivityNodeContainer != 0 && (refType == TypeOfReference.Reference || refType == TypeOfReference.Both))
            {
                references[ModelCode.CONNNODE_CONTAINER] = new List<long>();
                references[ModelCode.CONNNODE_CONTAINER].Add(connectivityNodeContainer);
            }

            if (topologicalNode != 0 && (refType == TypeOfReference.Reference || refType == TypeOfReference.Both))
            {
                references[ModelCode.CONNNODE_TOPOLONODE] = new List<long>();
                references[ModelCode.CONNNODE_TOPOLONODE].Add(topologicalNode);
            }

            base.GetReferences(references, refType);
        }

        public override void AddReference(ModelCode referenceId, long globalId)
        {
            switch (referenceId)
            {
                case ModelCode.TERMINAL_CONNNODE:
                    terminals.Add(globalId);
                    break;

                default:
                    base.AddReference(referenceId, globalId);
                    break;
            }
        }

        public override void RemoveReference(ModelCode referenceId, long globalId)
        {
            switch (referenceId)
            {
                case ModelCode.TERMINAL_CONNNODE:

                    if (terminals.Contains(globalId))
                    {
                        terminals.Remove(globalId);
                    }
                    else
                    {
                        CommonTrace.WriteTrace(CommonTrace.TraceWarning, "Entity (GID = 0x{0:x16}) doesn't contain reference 0x{1:x16}.", this.GlobalId, globalId);
                    }

                    break;

                default:
                    base.RemoveReference(referenceId, globalId);
                    break;
            }
        }

        #endregion IReference implementation
    }
}
