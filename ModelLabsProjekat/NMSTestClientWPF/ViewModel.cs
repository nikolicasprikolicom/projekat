﻿using FTN.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelventDMS.Services.NetworkModelService.TestClientWPF.Tests;

namespace NMSTestClientWPF
{
    public class ViewModel : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged implementation

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChangedEvent(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        public enum DMSType1 : short
        {
            NO_TYPE = 0x0000,
            BASEVOLTAGE = 0x0001,
            CONNNODECONTAINER = 0x0002,
            TOPOLONODE = 0x0003,
            CONNNODE = 0x0004,
            TERMINAL = 0x0005,
            SWITCH = 0x0006
        }

        private DMSType1 _selectedType;
        private long[] _gids;
        private long _selectedGid;
        private TestGda _testgda;
        private ModelCode[] _properties;
        private Association _association;
        private ModelCode[] _assocProperties;
        private ModelCode _selectedAssocProperty;
        private DMSType1 _selectedAssocType;
        private bool _isGetValuesChecked;
        private bool _isGetExtentValuesChecked;
        private bool _isGetRelatedValuesChecked;
        private string _executeResult;
        private bool _isGidEnabled;
        private bool _isAssocEnabled;
        private bool _isPropertiesEnabled;

        public DelegateCommand ExecuteCommand { get; set; }
        public string ExecuteResult
        {
            get
            {
                return _executeResult;
            }
            set
            {
                _executeResult = value;
                RaisePropertyChangedEvent("ExecuteResult");
            }
        }

        public ViewModel()
        {
            _testgda = new TestGda();
            ExecuteCommand = new DelegateCommand(new Action(Execute));
            _selectedGid = -1;
            IsGetValuesChecked = true;
            SelectedProperties = new List<ModelCode>();
            SelectedType = DMSType1.BASEVOLTAGE;
            _selectedAssocType = 0;
            SelectedAssocProperty = 0;
        }

        public DMSType1 SelectedAssocType
        {
            get
            {
                return _selectedAssocType;
            }
            set
            {
                _selectedAssocType = value;
                RaisePropertyChangedEvent("SelectedAssocType");
                if (_selectedAssocType == 0)
                {
                    HashSet<ModelCode> set = new HashSet<ModelCode>();
                    DMSType[] types = ((DMSType[])Enum.GetValues(typeof(DMSType)));
                    List<DMSType> typeslist = types.ToList<DMSType>();
                    typeslist.Remove(DMSType.MASK_TYPE);
                    foreach (var item in typeslist)
                    {
                        foreach (var item1 in _testgda.GetPropertiesOfType(item))
                        {
                            set.Add(item1);
                        }

                        //set.AddRange(_testgda.GetPropertiesOfType(item));
                    }
                    Properties = set.ToArray();
                }
                else
                {
                    Assoc = new Association(_selectedAssocProperty, _testgda.GetModelCodeFromType((DMSType)_selectedAssocType));
                    Properties = _testgda.GetPropertiesOfType((DMSType)_selectedAssocType).ToArray();
                }

            }
        }

        public Association Assoc
        {
            get
            {
                return _association;
            }
            set
            {
                _association = value;
                RaisePropertyChangedEvent("Assoc");
            }
        }

        public ModelCode SelectedAssocProperty
        {
            get
            {
                return _selectedAssocProperty;
            }
            set
            {
                _selectedAssocProperty = value;
                Assoc = new Association(_selectedAssocProperty, _selectedAssocType == 0 ? 0 : _testgda.GetModelCodeFromType((DMSType)_selectedAssocType));
                RaisePropertyChangedEvent("SelectedAssocProperty");
            }
        }

        public List<ModelCode> SelectedProperties
        {
            get;
            set;
        }

        public ModelCode[] AssocProperties
        {
            get
            {
                return _assocProperties;
            }
            set
            {
                _assocProperties = value;
                RaisePropertyChangedEvent("AssocProperties");
            }
        }

        private void Execute()
        {
            if (IsGetValuesChecked)
            {
                if (SelectedGid != -1)
                {
                    ResourceDescription rd = _testgda.GetValues(SelectedGid, SelectedProperties);
                    ExecuteResult = GetStringFromResourceDescription(rd);
                }
                else
                {
                    System.Windows.MessageBox.Show("Please select GID", "Error");
                }
            }
            else if (IsGetExtentValuesChecked)
            {
                List<ResourceDescription> rds = _testgda.GetExtentValues((DMSType)SelectedType, SelectedProperties);
                StringBuilder sb = new StringBuilder();
                foreach (var item in rds)
                {
                    sb.Append(GetStringFromResourceDescription(item));
                }
                ExecuteResult = sb.ToString();
            }
            else if (IsGetRelatedValuesChecked)
            {
                if (SelectedGid != -1)
                {
                    if (SelectedAssocProperty == 0)
                    {
                        System.Windows.MessageBox.Show("Please select association property", "Error");
                    }
                    else
                    {
                        try
                        {
                            List<ResourceDescription> rds = _testgda.GetRelatedValues(SelectedGid, SelectedProperties, Assoc);
                            StringBuilder sb = new StringBuilder();
                            foreach (var item in rds)
                            {
                                sb.Append(GetStringFromResourceDescription(item));
                            }
                            ExecuteResult = sb.ToString();
                        }
                        catch (Exception e)
                        {
                            ExecuteResult = "Error:\n" + e.Message;
                        }

                    }
                }
                else
                {
                    System.Windows.MessageBox.Show("Please select GID", "Error");
                }
            }
        }

        private string GetStringFromResourceDescription(ResourceDescription rd)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("GID: 0x");
            sb.AppendLine(rd.Id.ToString("X16"));
            sb.AppendLine("Properties: ");
            foreach (Property property in rd.Properties)
            {
                sb.Append(property.Id.ToString());
                sb.Append(" : ");
                if (property.GetValue() is List<long>)
                {
                    foreach (var item in (List<long>)property.GetValue())
                    {
                        sb.Append(item.ToString("X16"));
                        sb.Append(" ");
                    }
                    sb.AppendLine();
                }
                else
                {
                    sb.AppendLine(property.ToString());
                }
            }
            sb.AppendLine();
            return sb.ToString();
        }

        public long SelectedGid
        {
            get
            {
                return _selectedGid;
            }
            set
            {
                _selectedGid = value;
                RaisePropertyChangedEvent("SelectedGid");
            }
        }

        public DMSType1[] DMSTypes
        {
            get
            {
                return ((DMSType1[])Enum.GetValues(typeof(DMSType1)));
            }
        }

        public long[] GIDs
        {
            get
            {
                return _gids;
            }
            set
            {
                _gids = value;
                RaisePropertyChangedEvent("GIDs");
            }
        }

        public DMSType1 SelectedType
        {
            get { return _selectedType; }
            set
            {
                _selectedType = value;
                RaisePropertyChangedEvent("SelectedType");
                if (_selectedType != 0)
                {
                    GIDs = _testgda.GetGidsOfType((DMSType)_selectedType).ToArray();
                }
                else
                {
                    GIDs = (new List<long>()).ToArray();
                }

                if (_isGetRelatedValuesChecked)
                {
                    if (_selectedAssocType != 0)
                    {
                        Properties = _testgda.GetPropertiesOfType((DMSType)_selectedAssocType).ToArray();
                    }
                    else
                    {
                        HashSet<ModelCode> set = new HashSet<ModelCode>();
                        DMSType[] types = ((DMSType[])Enum.GetValues(typeof(DMSType)));
                        List<DMSType> typeslist = types.ToList<DMSType>();
                        typeslist.Remove(DMSType.MASK_TYPE);
                        foreach (var item in typeslist)
                        {
                            foreach (var item1 in _testgda.GetPropertiesOfType(item))
                            {
                                set.Add(item1);
                            }
                        }
                        Properties = set.ToArray();
                    }
                }
                else
                {
                    Properties = _testgda.GetPropertiesOfType((DMSType)_selectedType).ToArray();
                }
                ModelCode[] props = _testgda.GetPropertiesOfType((DMSType)_selectedType).ToArray();
                List<ModelCode> refProp = new List<ModelCode>();
                for (int i = 0; i < props.Length; i++)
                {
                    if (Property.GetPropertyType(props[i]) == PropertyType.Reference ||
                        Property.GetPropertyType(props[i]) == PropertyType.ReferenceVector)
                    {
                        refProp.Add(props[i]);
                    }
                }

                AssocProperties = refProp.ToArray();
                SelectedGid = -1;
            }
        }

        public ModelCode[] Properties
        {
            get
            {
                return _properties;
            }
            set
            {
                _properties = value;
                RaisePropertyChangedEvent("Properties");
            }
        }

        public bool IsGetValuesChecked
        {
            get
            {
                return _isGetValuesChecked;
            }
            set
            {
                _isGetValuesChecked = value;
                RaisePropertyChangedEvent("IsGetValuesChecked");
                if (_isGetValuesChecked)
                {
                    if (_selectedType != 0)
                    {
                        Properties = _testgda.GetPropertiesOfType((DMSType)_selectedType).ToArray();
                    }
                    IsAssocEnabled = false;
                    IsGidEnabled = true;
                    IsPropertiesEnabled = true;
                }
            }
        }

        public bool IsGetExtentValuesChecked
        {
            get
            {
                return _isGetExtentValuesChecked;
            }
            set
            {
                _isGetExtentValuesChecked = value;
                RaisePropertyChangedEvent("IsGetExtentValuesChecked");
                if (_isGetExtentValuesChecked)
                {
                    if (_selectedType != 0)
                    {
                        Properties = _testgda.GetPropertiesOfType((DMSType)_selectedType).ToArray();
                    }
                    IsAssocEnabled = false;
                    IsGidEnabled = false;
                    IsPropertiesEnabled = true;
                }
            }
        }

        public bool IsGetRelatedValuesChecked
        {
            get
            {
                return _isGetRelatedValuesChecked;
            }
            set
            {
                _isGetRelatedValuesChecked = value;
                RaisePropertyChangedEvent("IsGetRelatedValuesChecked");
                if (_isGetRelatedValuesChecked)
                {
                    if (_selectedAssocType != 0)
                    {
                        Properties = _testgda.GetPropertiesOfType((DMSType)_selectedAssocType).ToArray();
                    }
                    else
                    {
                        HashSet<ModelCode> set = new HashSet<ModelCode>();
                        DMSType[] types = ((DMSType[])Enum.GetValues(typeof(DMSType)));
                        List<DMSType> typeslist = types.ToList<DMSType>();
                        typeslist.Remove(DMSType.MASK_TYPE);
                        foreach (var item in typeslist)
                        {
                            foreach (var item1 in _testgda.GetPropertiesOfType(item))
                            {
                                set.Add(item1);
                            }
                        }
                        Properties = set.ToArray();
                    }
                    IsAssocEnabled = true;
                    IsGidEnabled = true;
                    IsPropertiesEnabled = true;
                }
            }
        }

        public bool IsGidEnabled
        {
            get
            {
                return _isGidEnabled;
            }
            set
            {
                _isGidEnabled = value;
                RaisePropertyChangedEvent("IsGidEnabled");
            }
        }

        public bool IsAssocEnabled
        {
            get
            {
                return _isAssocEnabled;
            }
            set
            {
                _isAssocEnabled = value;
                RaisePropertyChangedEvent("IsAssocEnabled");
            }
        }

        public bool IsPropertiesEnabled
        {
            get
            {
                return _isPropertiesEnabled;
            }
            set
            {
                _isPropertiesEnabled = value;
                RaisePropertyChangedEvent("IsPropertiesEnabled");
            }
        }

    }
}
