using System;
using System.Collections.Generic;
using System.Text;

namespace FTN.Common
{
	
	public enum DMSType : short
	{		
		MASK_TYPE							= unchecked((short)0xFFFF),

		BASEVOLTAGE							= 0x0001,
		CONNNODECONTAINER					= 0x0002,
		TOPOLONODE							= 0x0003,
		CONNNODE						    = 0x0004,
		TERMINAL							= 0x0005,
        SWITCH                              = 0x0006
	}

    [Flags]
	public enum ModelCode : long
	{
		IDOBJ								= 0x1000000000000000,
		IDOBJ_GID							= 0x1000000000000104,
		IDOBJ_ALIASNAME					    = 0x1000000000000207,
		IDOBJ_MRID							= 0x1000000000000307,
		IDOBJ_NAME							= 0x1000000000000407,	

		PSR									= 0x1100000000000000,

		EQUIPMENT							= 0x1110000000000000,
		EQUIPMENT_AGGREGATE				    = 0x1110000000000101,
		EQUIPMENT_NORMALLYINSERVICE			= 0x1110000000000201,		

		CONDEQ								= 0x1111000000000000,
		CONDEQ_BASVOLTAGE					= 0x1111000000000109,
        CONDEQ_TERMINALS                    = 0x1111000000000219,

        SWITCH                              = 0x1111100000060000,
        SWITCH_NORMALOPEN                   = 0x1111100000060101,
        SWITCH_RATEDCURRENT                 = 0x1111100000060205,
        SWITCH_RETAINED                     = 0x1111100000060301,
        SWITCH_SWITCHONCOUNT                = 0x1111100000060403,
        SWITCH_SWITCHONDATE                 = 0x1111100000060508,

        CONNNODECONTAINER                   = 0x1120000000020000,
        CONNNODECONTAINER_CONNNODES         = 0x1120000000020119,

        BASEVOLTAGE                         = 0x1200000000010000,
        BASEVOLTAGE_CONDEQS                 = 0x1200000000010119,

        TOPOLONODE                          = 0x1300000000030000,
        TOPOLONODE_CONNNODES                = 0x1300000000030119,

        CONNNODE                            = 0x1400000000040000,
        CONNNODE_CONTAINER                  = 0x1400000000040109,
        CONNNODE_TOPOLONODE                 = 0x1400000000040209,
        CONNNODE_TERMINALS                  = 0x1400000000040319,

        TERMINAL                            = 0x1500000000050000,
        TERMINAL_CONNNODE                   = 0x1500000000050109,
        TERMINAL_CONDEQ                     = 0x1500000000050209,
    }

    [Flags]
	public enum ModelCodeMask : long
	{
		MASK_TYPE			 = 0x00000000ffff0000,
		MASK_ATTRIBUTE_INDEX = 0x000000000000ff00,
		MASK_ATTRIBUTE_TYPE	 = 0x00000000000000ff,

		MASK_INHERITANCE_ONLY = unchecked((long)0xffffffff00000000),
		MASK_FIRSTNBL		  = unchecked((long)0xf000000000000000),
		MASK_DELFROMNBL8	  = unchecked((long)0xfffffff000000000),		
	}																		
}


